package com.precisionmedcare.jkkj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi
@SpringBootApplication
@MapperScan("com.precisionmedcare.jkkj.mapper")
public class BreastcancerWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(BreastcancerWebApplication.class, args);
    }

}
