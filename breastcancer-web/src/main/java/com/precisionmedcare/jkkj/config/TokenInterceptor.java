package com.precisionmedcare.jkkj.config;

import cn.hutool.core.util.StrUtil;
import com.precisionmedcare.jkkj.annotation.PassToken;
import com.precisionmedcare.jkkj.annotation.UserLoginToken;
import com.precisionmedcare.jkkj.controller.common.CommonUser;
import com.precisionmedcare.jkkj.util.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class TokenInterceptor implements HandlerInterceptor {
     private static final Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);
    @Resource
    JwtTokenUtil jwtTokenUtil;
    @Resource
    CommonUser commonUser;
    //不需要登录就可以访问的路径(比如:注册 swagger等)
    String[] includeUrls = new String[]{
            "/swagger-ui.html",
            "/swagger-ui/**",
            "/swagger-resources/**",
            "/images/**",
            "/webjars/**",
            "/v3/api-docs",
            "/configuration/ui",
            "/error",
            "/sw.js",
    };

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        String token = httpServletRequest.getHeader("Authorization");// 从 http 请求头中取出 token
        String URI = httpServletRequest.getRequestURI();
         logger.info("请求URI:" + URI);
        //是否需要过滤
        boolean needFilter = isNeedFilter(URI);
        //不需要登陆就能访问的uri
        if(!needFilter){
            return true;
        }else {
            // 未通过的api

            if (!StrUtil.isEmpty(token)) {
                token = token.replace("Bearer", "").trim();
            }
            // 如果不是映射到方法直接通过
            if(!(object instanceof HandlerMethod)){
                return true;
            }
            HandlerMethod handlerMethod=(HandlerMethod)object;
            Method method=handlerMethod.getMethod();
            //检查是否有passtoken注释，有则跳过认证
            if (method.isAnnotationPresent(PassToken.class)) {
                PassToken passToken = method.getAnnotation(PassToken.class);
                if (passToken.required()) {
                    return true;
                }
            }
            //检查有没有需要用户权限的注解
            if (method.isAnnotationPresent(UserLoginToken.class)) {
                UserLoginToken userLoginToken = method.getAnnotation(UserLoginToken.class);
                if (userLoginToken.required()) {
                    // 执行认证
                    if (token == null) {
                        throw new RuntimeException("无token，请重新登录");
                    }
                    //验证用户是否存在
                    return commonUser.checkUserAndVerifyToken(token);
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
    public boolean isNeedFilter(String uri) {
        for (String includeUrl : includeUrls) {
            if (uri.contains(includeUrl)) {
                return false;
            }
        }
        return true;
    }
}
