package com.precisionmedcare.jkkj.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author qlh
 * @return
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Value("${upload.path}")
    String path;

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/file/**").addResourceLocations("file:" + path + "/");
    }
}
