package com.precisionmedcare.jkkj.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("TinyGrey")
                .enable(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.precisionmedcare.jkkj.controller"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("TinyGrey-API接口")
                .description("更多请咨询服务开madison。")
                .contact(new Contact("madison", "http://www.tinygray.cn", "qlh2561808384@163.com"))
                .version("1.0")
                .license("tinygrey 2.0")
                .licenseUrl("http://www.tinygray.cn")
                .build();
    }
}
