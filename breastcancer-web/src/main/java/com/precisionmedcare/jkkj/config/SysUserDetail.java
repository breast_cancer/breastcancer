package com.precisionmedcare.jkkj.config;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.precisionmedcare.jkkj.entity.SysUser;
import com.precisionmedcare.jkkj.enums.UserStatusEnum;
import com.precisionmedcare.jkkj.service.SysUserService;
import com.precisionmedcare.jkkj.util.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
@Configuration
public class SysUserDetail {
     private static final Logger log = LoggerFactory.getLogger(SysUserDetail.class);
    @Resource
    SysUserService sysUserService;
    @Resource
    JwtTokenUtil jwtTokenUtil;
    @Bean
    public SysUserDetail generate(){
        log.info("我进来了");
        return new SysUserDetail();
    }
    public SysUser getUserInfo(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        if (!StrUtil.isEmpty(token)) {
            token = token.replace("Bearer", "").trim();
        }
        int userId = jwtTokenUtil.getUsernameAndUserIdFromToken(token);
        return sysUserService.getBaseMapper()
                .selectOne(
                        new QueryWrapper<SysUser>()
                                .lambda()
                                .eq(SysUser::getId, userId)
                                .eq(SysUser::getStatus, UserStatusEnum.NORMAL.getCode()
                                )
                );
    }
}
