package com.precisionmedcare.jkkj.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.precisionmedcare.jkkj.config.SysUserDetail;
import com.precisionmedcare.jkkj.entity.SysResult;
import com.precisionmedcare.jkkj.entity.SysUser;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import com.precisionmedcare.jkkj.service.SysResultDetailsService;
import com.precisionmedcare.jkkj.service.SysResultService;
import com.precisionmedcare.jkkj.util.ZipUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipOutputStream;

/**
 * @author qlh
 * @return
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
@Api(tags = "下载功能")
@RestController
@RequestMapping("download")
public class DownloadController extends ApiController {
    @Resource
    private SysUserDetail sysUserDetail;
    @Resource
    private SysResultService sysResultService;
    @Resource
    SysResultDetailsService sysResultDetailsService;
    @Value("${upload.path}")
    private String path;
    private static final String[] type = {"video","avatar","record.txt"};

    @ApiOperation(value = "文件下载", notes = "")
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
            @ApiResponse(code = 1001, message = "数组不能为空"),
    })
    @GetMapping("downloadHistory/{resultIds}")
    public R downloadHistory(HttpServletRequest request, HttpServletResponse response, @PathVariable List<Integer> resultIds) {
        SysUser userInfo = sysUserDetail.getUserInfo(request);
        Assert.isTrue(resultIds.size() != 0, ErrorCodeEnum.ARRAY_CANNOT_BE_EMPTY);
        StringBuilder message = new StringBuilder();
        SysResult result = null;
        String uuid = "";
        for (int resultId : resultIds) {
            result = sysResultService.getById(resultId);
            message.append("上传记录：").append(result.getId()).append("上传医生：").append(result.getUploaderUsername()).append("上传时间：").append(result.getUploadTime()).append("病人编号：").append(result.getPatientId()).append("检测编号：").append(result.getDetectionId()).append("检测结果：").append(result.getResult()).append("错误信息：").append(result.getErrorInfo()).append("备注：").append(result.getDoctorNotes());
            // 构建上传文件的存放 "文件夹" 路径
            uuid = result.getResult().substring(0, result.getResult().lastIndexOf("/")).substring(result.getResult().substring(0, result.getResult().lastIndexOf("/")).lastIndexOf("/") + 1);
            String fileDir = new String(path + File.separator + result.getUploaderUsername() + File.separator + type[0] + File.separator + uuid + File.separator + type[2]);
            File file = new File(fileDir);
            try {
                file.createNewFile();
                BufferedWriter out = new BufferedWriter(new FileWriter(file));
                out.write(message.toString());
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
                return success(false);
            }
            message = new StringBuilder();

            String zipName = "record" + resultId + ".zip";
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
            try {
                ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
                String DirPath = path + File.separator + Objects.requireNonNull(result).getUploaderUsername() + File.separator + type[0] + File.separator + uuid;
                ZipUtils.doCompress(DirPath, out);
            } catch (IOException e) {
                e.printStackTrace();
                return success(false);
            }
        }
        return success(true);
    }
}
