package com.precisionmedcare.jkkj.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.precisionmedcare.jkkj.annotation.PassToken;
import com.precisionmedcare.jkkj.annotation.UserLoginToken;
import com.precisionmedcare.jkkj.consts.CommonConst;
import com.precisionmedcare.jkkj.controller.common.CommonResult;
import com.precisionmedcare.jkkj.dto.SysResultDTO;
import com.precisionmedcare.jkkj.entity.SysResult;
import com.precisionmedcare.jkkj.entity.SysResultDetails;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import com.precisionmedcare.jkkj.service.SysResultDetailsService;
import com.precisionmedcare.jkkj.service.SysResultService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * (SysResult)表控制层
 *
 * @author madison
 * @since 2021-01-04 11:25:52
 */
@Api(tags = "乳腺癌计算")
@RestController
@RequestMapping("sysResult")
public class SysResultController extends ApiController {
     private static final Logger log = LoggerFactory.getLogger(SysResultController.class);
    /**
     * 服务对象
     */
    @Resource
    private SysResultService sysResultService;
    @Resource
    SysResultDetailsService sysResultDetailsService;
    @Resource
    private CommonResult commonResult;

    /**
     * 分页查询所有数据
     *
     * @param page      分页对象
     * @param sysResult 查询实体
     * @return 所有数据
     */
    @UserLoginToken
    @GetMapping("getResult")
    @ApiOperation(value = "查询所有分析结果",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "用于分页-当前页(可传可不传---不传的话默认第1页)", required = false, dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "size", value = "用于分页-当前页展示数据条数(可传可不传---不传默认一页10条数据)", required = false, dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "patientId", value = "病人id(查询条件)", required = false, dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "detectionId", value = "检测id(查询条件)", required = false, dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "用户id(用户登进去之后 分析结果初始化条件)", required = false, dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "uploaderUsername", value = "上传用户名(查询条件)", required = false, dataTypeClass = String.class, paramType = "query"),
    })
    @ApiResponses({
            @ApiResponse(code = 0, message = "执行成功"),
            @ApiResponse(code = -1, message = "操作失败")
    })
    public R selectAll(@ApiIgnore Page<SysResult> page, @ApiIgnore SysResult sysResult) {
        return success(sysResultService
                .page(page, new QueryWrapper<>(sysResult)
                        .eq("status", CommonConst.GLOBAL_NORMAL_STATE)
                        .orderByDesc("upload_time")
                        .eq("error_info", "")
                )
        );
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.sysResultService.getById(id));
    }

    /**
     * 开始计算
     *
     * @param sysResultDTO 实体对象
     * @return 计算结果
     */
    @UserLoginToken
    @PostMapping
    @ApiOperation(value = "开始计算功能", notes = "")
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功（不能以0作为判断成功标准）"),
            @ApiResponse(code = 1001, message = "数组不能为空"),
            @ApiResponse(code = 1002, message = "对象不能为空"),
            @ApiResponse(code = 1003, message = "业务代码true通过"),
    })
    public R calculate(@RequestBody SysResultDTO sysResultDTO, HttpServletRequest request) {
        return commonResult.calculate(sysResultDTO, request);
    }

    /**
     * 修改数据
     *
     * @param sysResult 实体对象
     * @return 修改结果
     */
    @ApiOperation(value = "更新结果记录表", notes = "")
    @PutMapping
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
    })
    public R update(@RequestBody SysResult sysResult) {
        return success(this.sysResultService.updateById(sysResult));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @UserLoginToken
    @DeleteMapping
    @ApiOperation(value = "根据id删除分析结果-支持批量",notes = "参数是list集合里面放对象")
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
            @ApiResponse(code = 1003, message = "业务代码true通过"),
    })
    public R delete(@RequestParam("idList") List<Long> idList) {
        boolean flag = this.sysResultService.removeByIds(idList);
        Assert.isTrue(flag, ErrorCodeEnum.IS_TRUE_PASS);
        boolean remove = false;
        for (Long id : idList) {
            remove = sysResultDetailsService.remove(new QueryWrapper<SysResultDetails>().lambda().eq(SysResultDetails::getResultId, id));
            Assert.isTrue(remove, ErrorCodeEnum.IS_TRUE_PASS);
        }
        return success(flag && remove);
    }

    @UserLoginToken
    @PutMapping("deleteResultById")
    @ApiOperation(value = "根据id删除分析结果-支持批量",notes = "参数是list集合里面放对象")
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
    })
    public R deleteResult(@RequestBody List<SysResult> idList) {
        return success(sysResultService.updateBatchById(idList));
    }
}
