package com.precisionmedcare.jkkj.controller.common;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.precisionmedcare.jkkj.dto.SysResultDTO;
import com.precisionmedcare.jkkj.entity.SysResult;
import com.precisionmedcare.jkkj.entity.SysResultDetails;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import com.precisionmedcare.jkkj.service.SysResultDetailsService;
import com.precisionmedcare.jkkj.service.SysResultService;
import com.precisionmedcare.jkkj.util.AlgoUdp;
import com.precisionmedcare.jkkj.util.IpUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;

import static com.baomidou.mybatisplus.extension.api.Assert.*;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
@Component
public class CommonResult {
    @Resource
    private SysResultService sysResultService;
    @Resource
    private SysResultDetailsService sysResultDetailsService;

    public R calculate(SysResultDTO sysResultDTO, HttpServletRequest request) {
        SysResult sysResult = new SysResult();
        Assert.isTrue(sysResultDTO != null, ErrorCodeEnum.OBJECT_CANNOT_BE_EMPTY);
        sysResultDTO.setUploadTime(DateUtil.date());
        sysResultDTO.setUserIp(IpUtil.getClientIpAddress(request));
        BeanUtils.copyProperties(sysResultDTO, sysResult);
        boolean resultFlag = sysResultService.save(sysResult);
        isTrue(resultFlag, ErrorCodeEnum.IS_TRUE_PASS);
       return saveResultDetails(sysResult);
    }

    private R executePyGetResult(SysResult sysResult) {
/*        JSONObject executePythonResult = null;
        JSONObject jsonObject = null;
        jsonObject = JSONUtil.createObj();
        jsonObject.putOnce("arg01", sysResult.getId());
        jsonObject.putOnce("patient_id", sysResult.getPatientId());
        jsonObject.putOnce("detect_id", sysResult.getDetectionId());
        jsonObject.putOnce("list", sysResult.getFilePath());
        jsonObject.putOnce("username", sysResult.getUploaderUsername());
        String udpReturnObject = AlgoUdp.getUdpBLKAlgo(jsonObject);
        executePythonResult = JSONUtil.parseObj(udpReturnObject);
        String result = executePythonResult.get("result").toString();
        String error = executePythonResult.get("error").toString();
        String arg01 = executePythonResult.get("arg01").toString();
        String patient_id = executePythonResult.get("patient_id").toString();
        String detect_id = executePythonResult.get("detect_id").toString();*/
        JSONObject object = JSONUtil.createObj();
        object.putOnce("arg01", sysResult.getId());
        object.putOnce("patient_id", sysResult.getPatientId());
        object.putOnce("detect_id", sysResult.getDetectionId());
        object.putOnce("result", sysResult.getFilePath().get(0));
        object.putOnce("error", "");
        if ("".equals(object.get("error"))) {
            sysResult.setResult(object.get("result").toString());
            sysResult.setErrorInfo(object.get("error").toString());
        }else {
            sysResult.setErrorInfo(object.get("error").toString());
        }
        boolean flag = sysResultService.updateById(sysResult);
        Assert.isTrue(flag, ErrorCodeEnum.IS_TRUE_PASS);
        return R.ok(sysResult);
        //判断py返回结果 是否有错误
    }

    private R saveResultDetails(SysResult sysResult) {
        List<SysResultDetails> targetList = new ArrayList<>();
        SysResultDetails sysResultDetails = new SysResultDetails();
        Assert.notNull(ErrorCodeEnum.ARRAY_CANNOT_BE_EMPTY, sysResult.getFilePath());
        for (String path : sysResult.getFilePath()) {
            sysResultDetails.setResultId(sysResult.getId());
            sysResultDetails.setUploadPath(path);
            targetList.add(sysResultDetails);
        }
        isTrue(sysResultDetailsService.saveBatch(targetList), ErrorCodeEnum.IS_TRUE_PASS);
        return executePyGetResult(sysResult);
    }
}
