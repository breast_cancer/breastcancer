package com.precisionmedcare.jkkj.controller.common;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.precisionmedcare.jkkj.dto.SysIcCardDTO;
import com.precisionmedcare.jkkj.entity.SysIcCard;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import com.precisionmedcare.jkkj.service.SysIcCardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
@Component
public class CommonIcCard {
     private static final Logger log = LoggerFactory.getLogger(CommonIcCard.class);
    @Resource
    SysIcCardService sysIcCardService;

    public boolean checkIcCardExist(String ic) {
        Integer count = sysIcCardService.getBaseMapper().selectCount(new QueryWrapper<SysIcCard>().lambda().eq(SysIcCard::getNumber, ic));
        return count == 0;
    }

    public R add(SysIcCard sysIcCard) {
        return R.ok(sysIcCardService.save(sysIcCard));
    }

    public R update(SysIcCard sysIcCard) {
        if (sysIcCard.getUserId() != 0) {
            SysIcCard targetSysIcCard = selectSysIcCardByUserId(sysIcCard.getUserId());
            if (targetSysIcCard != null) {
                targetSysIcCard.setUserId(0);
                sysIcCardService.updateById(targetSysIcCard);
            }
        }
        return R.ok(sysIcCardService.saveOrUpdate(sysIcCard));
    }

    public SysIcCard selectSysIcCardByUserId(int userId) {
        return sysIcCardService.getBaseMapper().selectOne(new QueryWrapper<SysIcCard>().lambda().eq(SysIcCard::getUserId, userId));
    }

    public SysIcCard selectSysIcCardByIcNumber(String ic) {
        return sysIcCardService.getBaseMapper().selectOne(new QueryWrapper<SysIcCard>().lambda().eq(SysIcCard::getNumber, ic));
    }

    public R checkIcCardByUserId(int userId) {
        SysIcCard sysIcCard = selectSysIcCardByUserId(userId);
        if (sysIcCard == null) {
            return R.failed(ErrorCodeEnum.UNBOUND_IC_CARD);
        }else {
            if (sysIcCard.getCount() > 0) {
                return R.ok(sysIcCard);
            }else {
                return R.failed(ErrorCodeEnum.JK_CARD_TIMES_USER_UP);
            }
        }
    }

    public R reduceIcNumber(int userId) {
        SysIcCard sysIcCard = selectSysIcCardByUserId(userId);
        sysIcCard.setCount(sysIcCard.getCount() - 1);
        return R.ok(sysIcCardService.updateById(sysIcCard));
    }

    public R beforeBindCheck(SysIcCardDTO sysIcCardDTO) {
        boolean update = false;
        if (checkIcCardExist(sysIcCardDTO.getNumber())) {
            return R.failed(ErrorCodeEnum.IC_DOES_NOT_EXIST);
        }else {
            SysIcCard targetSysIcCard = selectSysIcCardByIcNumber(sysIcCardDTO.getNumber());
            if(targetSysIcCard.getUserId() != 0){
                log.info("该ic卡已经绑定了用户，该用户id为：" + targetSysIcCard.getUserId());
                return R.failed(ErrorCodeEnum.JK_CARD_HAVE_ALREADY_BIND);
            }
            SysIcCard sourceSysIcCard = selectSysIcCardByUserId(sysIcCardDTO.getUserId());
            if (sourceSysIcCard != null) {
                sourceSysIcCard.setUserId(0);
                boolean b = sysIcCardService.updateById(sourceSysIcCard);
                Assert.isTrue(b, ErrorCodeEnum.IS_TRUE_PASS);
            }
            update = sysIcCardService.update(
                    new UpdateWrapper<SysIcCard>().
                            lambda().
                            eq(SysIcCard::getNumber, sysIcCardDTO.getNumber()).
                            set(SysIcCard::getUserId, sysIcCardDTO.getUserId())
            );
        }
        return R.ok(update);
    }
}
