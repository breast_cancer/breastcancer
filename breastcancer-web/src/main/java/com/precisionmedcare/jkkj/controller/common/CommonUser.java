package com.precisionmedcare.jkkj.controller.common;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.precisionmedcare.jkkj.dto.SysUserDTO;
import com.precisionmedcare.jkkj.entity.SysUser;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import com.precisionmedcare.jkkj.enums.UserStatusEnum;
import com.precisionmedcare.jkkj.service.SysUserService;
import com.precisionmedcare.jkkj.util.JwtTokenUtil;
import com.precisionmedcare.jkkj.util.PasswordUtil;
import com.precisionmedcare.jkkj.vo.SysUserVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */

/**
 *  @Component 注解表明一个类会作为组件类，并告知Spring要为这个类创建bean。
 *  @Component 作用于类
 *  @Bean 作用于方法
 */
@Component
public class CommonUser {
     private static final Logger logger = LoggerFactory.getLogger(CommonUser.class);
    @Resource
    SysUserService sysUserService;
    @Resource
    JwtTokenUtil jwtTokenUtil;

    public SysUser doesTheUsernameExist(SysUserDTO userDTO) {
        return sysUserService.getBaseMapper()
                .selectOne(
                        new QueryWrapper<SysUser>()
                                .lambda()
                                .eq(SysUser::getUsername, userDTO.getUsername())
                                .eq(SysUser::getStatus, UserStatusEnum.NORMAL.getCode())
                );
    }
    public R login(SysUserDTO userDTO){
        SysUser user = doesTheUsernameExist(userDTO);
        SysUserVo userVo = null;
        if (StrUtil.isEmptyIfStr(user)) {
            logger.info("该用户名：[" + userDTO.getUsername() + "]不存在！请输入正确的用户名");
            return R.failed(ErrorCodeEnum.INCORRECT_ACCOUNT);
        }else{
            try {
                String decryptPassword = PasswordUtil.decrypt(user.getPassword(), userDTO.getUsername());
                if (userDTO.getPassword().equals(decryptPassword)) {
                    String token = jwtTokenUtil.generateToken(user);
                    userVo = new SysUserVo(user, token);
                    return R.ok(userVo);
                }else {
                    logger.info("该用户名的密码：[" + userDTO.getPassword() + "]不正确！请输入正确的密码");
                    return R.failed(ErrorCodeEnum.INCORRECT_PASSWORD);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return R.failed(ErrorCodeEnum.PASSWORD_DECRYPTION_FAILED);
            }
        }
    }

    public boolean checkUserAndVerifyToken(String token) {
        int userId = jwtTokenUtil.getUsernameAndUserIdFromToken(token);
        SysUser user = sysUserService.getBaseMapper().selectOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getId, userId).eq(SysUser::getStatus, UserStatusEnum.NORMAL.getCode()));
        if (user == null) {
            throw new RuntimeException("用户不存在，请重新登录");
        }
        try {
            return jwtTokenUtil.validateToken(token, user);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("401");
        }
    }
}
