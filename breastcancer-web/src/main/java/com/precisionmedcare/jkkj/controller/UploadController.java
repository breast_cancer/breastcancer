package com.precisionmedcare.jkkj.controller;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.precisionmedcare.jkkj.annotation.PassToken;
import com.precisionmedcare.jkkj.config.SysUserDetail;
import com.precisionmedcare.jkkj.dto.UploadDTO;
import com.precisionmedcare.jkkj.entity.SysUser;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
@PassToken
@RestController
@RequestMapping("upload")
@Api(tags = "文件上传")
public class UploadController extends ApiController {
     private static final Logger log = LoggerFactory.getLogger(UploadController.class);
    @Value("${upload.path}")
    private String filePath;
    @Resource
    SysUserDetail sysUserDetail;

    @PostMapping("upload")
    @ApiOperation(value = "文件上传", notes = "")
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
            @ApiResponse(code = 1003, message = "业务代码true通过"),
            @ApiResponse(code = 1004, message = "上传文件为空"),
            @ApiResponse(code = 1005, message = "文件格式错误")
    })
    public R upload(HttpServletRequest request, @ModelAttribute UploadDTO uploadDTO) {
        String msg = "";
        SysUser userInfo = sysUserDetail.getUserInfo(request);
        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartHttpServletRequest.getFileMap();
        if ("avatar".equals(uploadDTO.getStatus())) {
            int i = uploadDTO.getOldAvatar().lastIndexOf("/");
            if (-1 != i) {
                String substring = uploadDTO.getOldAvatar().substring(i + 1);
                File file = new File(filePath + File.separator + userInfo.getUsername() + File.separator + uploadDTO.getStatus() + File.separator + substring);
                if(file.exists()){
                    boolean delete = file.delete();
                    Assert.isTrue(delete, ErrorCodeEnum.IS_TRUE_PASS);
                }
            }
        }
        for (MultipartFile file : fileMap.values()) {
            Assert.notNull(ErrorCodeEnum.UPLOAD_FILE_IS_EMPTY, file);
            String originalFilename = Objects.requireNonNull(file.getOriginalFilename()).replace(" ", "_");
            int i = originalFilename.lastIndexOf(".");
            if (-1 == i) {
                Assert.fail(ErrorCodeEnum.FILE_FORM_ERROR);
            } else {
                originalFilename = originalFilename.replace(originalFilename.substring(0, i), RandomUtil.randomString(10));
            }
            File fileDir = new File(filePath + File.separator + userInfo.getUsername() + File.separator + uploadDTO.getStatus() + File.separator + uploadDTO.getUuid() + File.separator);

            if (!fileDir.exists()) {
                boolean mkdir = fileDir.mkdirs();
                Assert.isTrue(mkdir, ErrorCodeEnum.IS_TRUE_PASS);
            }
            File newFile = new File(fileDir + File.separator + originalFilename);
            try {
                file.transferTo(newFile);
                if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
                    msg = "http://localhost:8899/file/" + userInfo.getUsername() + '/' + uploadDTO.getStatus() + '/' + uploadDTO.getUuid() + '/' + originalFilename;
                } else {
                    msg = "http://pmdcare.cn:82/breastcancer/" + userInfo.getUsername() + '/' + uploadDTO.getStatus() + '/' + uploadDTO.getUuid() + '/' + originalFilename;
                }
            } catch (IOException e) {
                e.printStackTrace();
                log.error("上传失败，失败原因：" + e.getMessage());
                return failed("上传失败，失败原因：" + e.getMessage());
            }
        }
        return success(msg);
    }
}
