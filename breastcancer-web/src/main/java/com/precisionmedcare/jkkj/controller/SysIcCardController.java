package com.precisionmedcare.jkkj.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.precisionmedcare.jkkj.annotation.UserLoginToken;
import com.precisionmedcare.jkkj.controller.common.CommonIcCard;
import com.precisionmedcare.jkkj.dto.SysIcCardDTO;
import com.precisionmedcare.jkkj.entity.SysIcCard;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import com.precisionmedcare.jkkj.service.SysIcCardService;
import io.swagger.annotations.*;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * (SysIcCard)表控制层
 *
 * @author madison
 * @since 2020-12-28 14:48:51
 */
@Api(tags = "精康检测卡")
@RestController
@RequestMapping("sysIcCard")
public class SysIcCardController extends ApiController {
     private static final Logger log = LoggerFactory.getLogger(SysIcCardController.class);
    /**
     * 服务对象
     */
    @Resource
    private SysIcCardService sysIcCardService;
    @Resource
    CommonIcCard commonIcCard;

    /**
     * 分页查询所有数据
     *
     * @param page      分页对象
     * @param sysIcCard 查询实体
     * @return 所有数据
     */
    @GetMapping("getAllIcList")
    @ApiOperation(value = "查询所有入库的精康检测卡",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "current:当前页(可传可不传---不传的话默认第1页)", required = false, dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "size", value = "size:当前页展示数据条数(可传可不传---不传默认一页10条数据)", required = false, dataTypeClass = Long.class, paramType = "query"),
    })
    @ApiResponses({
            @ApiResponse(code = 0, message = "执行成功"),
            @ApiResponse(code = -1, message = "操作失败")
    })
    public R selectAll(@ApiIgnore Page<SysIcCard> page, @ApiIgnore SysIcCard sysIcCard) {
        return success(this.sysIcCardService.page(page, new QueryWrapper<>(sysIcCard)));
    }

    /**
     * 新增数据
     *
     * @param sysIcCardDTO 实体对象
     * @return 新增结果
     */
    @UserLoginToken
    @PostMapping("saveOrUpdate")
    @ApiOperation(value = "新增或修改精康检测卡信息",notes = "")
    @ApiResponses({
            @ApiResponse(code = 0, message = "执行成功"),
            @ApiResponse(code = -1, message = "操作失败"),
            @ApiResponse(code = 1002, message = "对象不能为空"),
            @ApiResponse(code = 1100, message = "ic卡已经存在"),
    })
    public R saveOrUpdate(@RequestBody SysIcCardDTO sysIcCardDTO) {
        Assert.notNull(ErrorCodeEnum.OBJECT_CANNOT_BE_EMPTY,sysIcCardDTO);
        SysIcCard sysIcCard = new SysIcCard();
        if(commonIcCard.checkIcCardExist(sysIcCardDTO.getNumber())){
            BeanUtils.copyProperties(sysIcCardDTO, sysIcCard);
            return commonIcCard.add(sysIcCard);
        }else {
            BeanUtils.copyProperties(sysIcCardDTO, sysIcCard);
            return commonIcCard.update(sysIcCard);
        }
    }

    @UserLoginToken
    @ApiOperation(value = "用户绑定精康检测卡", notes = "传参为实体类")
    @ApiResponses({
            @ApiResponse(code = 0, message = "执行成功"),
            @ApiResponse(code = -1, message = "操作失败"),
            @ApiResponse(code = 1000, message = "属性为null或空"),
            @ApiResponse(code = 1100, message = "ic卡不存在"),
            @ApiResponse(code = 1006, message = "该ic卡已经绑定了用户"),
            @ApiResponse(code = 1003, message = "业务代码true通过"),
            @ApiResponse(code = 1103, message = "ic卡已经被别的用户绑定"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "number", value = "ic卡号", required = true, dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "登陆用户id", required = true, dataTypeClass = Integer.class, paramType = "query")
    })
    @PutMapping("userBindIcCard")
    public R userBindIcCard(@ApiIgnore @RequestBody SysIcCardDTO sysIcCardDTO) {
        Assert.notNull(ErrorCodeEnum.ATTRIBUTE_IS_NULL, sysIcCardDTO.getNumber(), sysIcCardDTO.getUserId());
        return commonIcCard.beforeBindCheck(sysIcCardDTO);
    }
    @UserLoginToken
    @GetMapping("checkIcCardByUserId/{userId}")
    @ApiOperation(value = "开始计算前检测精康检测卡次数是否用完",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "userId:登陆用户id", required = true, dataTypeClass = Integer.class, paramType = "path")
    })
    @ApiResponses({
            @ApiResponse(code=0,message = "执行成功"),
            @ApiResponse(code=1101,message = "该用户未绑定精康检测卡"),
            @ApiResponse(code=1102,message = "精康检测卡次数已用完，请联系管管理员")
    })
    public R checkIcCardByUserId(@PathVariable int userId) {
        return commonIcCard.checkIcCardByUserId(userId);
    }
    @UserLoginToken
    @PutMapping("reduceIcNumber")
    @ApiOperation("计算成功之后-精康检测卡次数修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "userId:登陆用户id", required = true, dataTypeClass = Integer.class, paramType = "path")
    })
    @ApiResponses({
            @ApiResponse(code=0,message = "执行成功"),
            @ApiResponse(code=-1,message = "操作失败")
    })
    public R reduceIcNumber(@Ignore @RequestBody SysIcCardDTO sysIcCardDTO){
        return commonIcCard.reduceIcNumber(sysIcCardDTO.getUserId());
    }
}
