package com.precisionmedcare.jkkj.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.precisionmedcare.jkkj.annotation.PassToken;
import com.precisionmedcare.jkkj.annotation.UserLoginToken;
import com.precisionmedcare.jkkj.consts.CommonConst;
import com.precisionmedcare.jkkj.controller.common.CommonUser;
import com.precisionmedcare.jkkj.dto.SysUserDTO;
import com.precisionmedcare.jkkj.entity.SysUser;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import com.precisionmedcare.jkkj.service.SysUserService;
import com.precisionmedcare.jkkj.util.PasswordUtil;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (SysUser)表控制层
 *
 * @author madison
 * @since 2020-12-11 17:05:08
 */
@Api(tags = "用户相关")
@RestController
@RequestMapping("sysUser")
public class SysUserController extends ApiController {
     private static final Logger log = LoggerFactory.getLogger(SysUserController.class);
    /**
     * 服务对象
     */
    @Resource
    private SysUserService sysUserService;
    @Resource
    private CommonUser commonUser;

    /**
     * 分页查询所有数据
     *
     * @param page    分页对象
     * @param sysUser 查询实体
     * @return 所有数据
     */
    @UserLoginToken
    @GetMapping("getAllUserList")
    @ApiOperation(value = "查询所有用户",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "current:当前页(可传可不传---不传的话默认第1页)", required = false, dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "size", value = "size:当前页展示数据条数(可传可不传---不传默认一页10条数据)", required = false, dataTypeClass = Long.class, paramType = "query"),
    })
    @ApiResponses({
            @ApiResponse(code = 0, message = "执行成功"),
            @ApiResponse(code = -1, message = "操作失败")
    })
    public R selectAll(Page<SysUser> page, SysUser sysUser) {
        return success(this.sysUserService.page(page, new QueryWrapper<>(sysUser).eq("status", CommonConst.GLOBAL_NORMAL_STATE)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @UserLoginToken
    @GetMapping("{id}")
    @ApiOperation(value = "根据用户id查询用户",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "id:被查询的用户id",required = true,paramType = "path",dataTypeClass = Serializable.class)
    })
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
    })
    public R selectOne(@PathVariable Serializable id) {
        return success(this.sysUserService.getById(id));
    }

    /**
     * 删除用户
     *
     * @param idList 主键结合
     * @return 修改结果
     */
    @PutMapping
    @ApiOperation(value = "删除用户", notes = "<span style='color:red;'>详细描述：</span>&nbsp;删系统用户接口。")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "idList", value = "idList:需要删除的用户主键id数组", paramType = "query", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "status", value = "status:需要删除的用户状态", paramType = "query", dataTypeClass = Integer.class, required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
            @ApiResponse(code = 1001, message = "数组不能为空")
    })
    public R update(@ApiIgnore @RequestBody List<SysUser> idList) {
        Assert.notNull(ErrorCodeEnum.ARRAY_CANNOT_BE_EMPTY, idList);
        boolean update = sysUserService.updateBatchById(idList);
        return success(update);
    }

    @PassToken
    @PostMapping("login")
    @ApiOperation(value = "登陆接口", notes = "<span style='color:red;'>详细描述：</span>&nbsp;系统登陆接口。")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "username：用户名", paramType = "query", required = true, dataTypeClass = String.class),
            @ApiImplicitParam(name = "password", value = "password：密码", paramType = "query", required = true, dataTypeClass = String.class)
    })
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
            @ApiResponse(code = 1052, message = "账号不正确"),
            @ApiResponse(code = 1053, message = "密码解密失败"),
            @ApiResponse(code = 1054, message = "密码不正确"),
    })
    public R login(@ApiIgnore @RequestBody SysUserDTO userDTO) {
        Assert.notNull(ErrorCodeEnum.ATTRIBUTE_IS_NULL, userDTO.getUsername(), userDTO.getPassword());
        return commonUser.login(userDTO);
    }

    @PassToken
    @PostMapping("registerOrUpdate")
    @ApiOperation(value = "注册-更新用户信息接口", notes = "<span style='color:red;'>详细描述：</span>&nbsp;系统预留注册接口。")
   /* @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id：用户表主键id（注册时默认为0可不传，修改时这个参数是存在的不为0）", paramType = "query", required = false, dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "username", value = "username：用户名", paramType = "query", required = true, dataTypeClass = String.class),
            @ApiImplicitParam(name = "password", value = "password：密码", paramType = "query", required = true, dataTypeClass = String.class),
            @ApiImplicitParam(name = "nickname", value = "nickname：昵称", paramType = "query", required = false, dataTypeClass = String.class),
            @ApiImplicitParam(name = "remark", value = "remark：备注", paramType = "query", required = false, dataTypeClass = String.class),
            @ApiImplicitParam(name = "avatar", value = "avatar：头像", paramType = "query", required = false, dataTypeClass = String.class),
            @ApiImplicitParam(name = "userType", value = "avatar：用户类型（默认权限USER）", paramType = "query", required = false, dataTypeClass = String.class),
            @ApiImplicitParam(name = "status", value = "status：用户状态(0正常1禁用-默认0)", paramType = "query", required = false, dataTypeClass = Integer.class),
    })*/
    @ApiResponses({
            @ApiResponse(code = 0, message = "操作成功"),
            @ApiResponse(code = -1, message = "执行失败"),
            @ApiResponse(code = 1000, message = "实体类参数为null"),
            @ApiResponse(code = 1050, message = "用户名已经存在"),
            @ApiResponse(code = 1051, message = "密码加密失败"),
    })
    public R registerOrUpdate(@RequestBody SysUserDTO userDTO) {
        boolean flag = false;
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(userDTO, sysUser);
        if (userDTO.getId() == 0) {
            try {
                sysUser.setPassword(PasswordUtil.encrypt(sysUser.getPassword(), sysUser.getUsername()));
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("密码加密失败");
                return R.failed(ErrorCodeEnum.PASSWORD_ENCRYPTION_FAILED);
            }
            if (StrUtil.isEmptyIfStr(commonUser.doesTheUsernameExist(userDTO))) {
                if(StrUtil.isEmptyIfStr(userDTO.getNickname())){
                    sysUser.setNickname(userDTO.getUsername());
                }
                if(StrUtil.isEmptyIfStr(userDTO.getAvatar())){
                    sysUser.setAvatar("http://pmdcare.cn:82/default/default.jpg");
                }
                flag = sysUserService.save(sysUser);
            } else {
                logger.info("该用户名：[" + sysUser.getUsername() + "]已经存在！请更改用户名");
                return R.failed(ErrorCodeEnum.USERNAME_ALREADY_EXISTS);
            }
        } else {
            if (StrUtil.isEmpty(sysUser.getPassword())) {
                sysUser.setPassword(null);
            } else {
                try {
                    sysUser.setPassword(PasswordUtil.encrypt(sysUser.getPassword(), sysUser.getUsername()));
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("密码加密失败");
                    return R.failed(ErrorCodeEnum.PASSWORD_ENCRYPTION_FAILED);
                }
            }
            flag = sysUserService.saveOrUpdate(sysUser);
        }
        return success(flag);
    }
}
