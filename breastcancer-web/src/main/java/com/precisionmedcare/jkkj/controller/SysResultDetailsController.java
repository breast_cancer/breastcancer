package com.precisionmedcare.jkkj.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.precisionmedcare.jkkj.annotation.UserLoginToken;
import com.precisionmedcare.jkkj.entity.SysResultDetails;
import com.precisionmedcare.jkkj.enums.ErrorCodeEnum;
import com.precisionmedcare.jkkj.service.SysResultDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (SysResultDetails)表控制层
 *
 * @author madison
 * @since 2021-01-04 14:22:02
 */
@Api(tags = "乳腺癌计算明细")
@RestController
@RequestMapping("sysResultDetails")
public class SysResultDetailsController extends ApiController {
     private static final Logger log = LoggerFactory.getLogger(SysResultDetailsController.class);
    /**
     * 服务对象
     */
    @Resource
    private SysResultDetailsService sysResultDetailsService;

    /**
     * 分页查询所有数据
     *
     * @param page             分页对象
     * @param sysResultDetails 查询实体
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<SysResultDetails> page, SysResultDetails sysResultDetails) {
        return success(this.sysResultDetailsService.page(page, new QueryWrapper<>(sysResultDetails)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.sysResultDetailsService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param sysResultDetails 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody SysResultDetails sysResultDetails) {
        return success(this.sysResultDetailsService.save(sysResultDetails));
    }

    /**
     * 修改数据
     *
     * @param sysResultDetails 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody SysResultDetails sysResultDetails) {
        return success(this.sysResultDetailsService.updateById(sysResultDetails));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestParam("idList") List<Long> idList) {
        return success(this.sysResultDetailsService.removeByIds(idList));
    }

    @UserLoginToken
    @GetMapping("getResultDetailByResultId/{resultId}")
    @ApiOperation(value = "获取结果详情", notes = "根据结果id查询详情")
    @ApiResponses({
            @ApiResponse(responseCode = "0",description = "操作成功"),
            @ApiResponse(responseCode = "1001",description = "数组不能为空"),
    })
    public R getResultDetailByResultId(@PathVariable Integer resultId) {
        List<SysResultDetails> list = sysResultDetailsService.list(new QueryWrapper<SysResultDetails>().lambda().eq(SysResultDetails::getResultId, resultId));
        Assert.isTrue(!list.isEmpty(), ErrorCodeEnum.ARRAY_CANNOT_BE_EMPTY);
        return success(list);
    }
}
