package com.precisionmedcare.jkkj.enums;
/**
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @creed: Talk is cheap,show me the code
 * @date
 */
public enum UserStatusEnum {
    NORMAL(1, "正常"),
    DISABLE(0, "禁用"),;
    private Integer code;
    private String desc;

    UserStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
