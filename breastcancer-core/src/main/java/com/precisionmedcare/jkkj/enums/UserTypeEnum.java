package com.precisionmedcare.jkkj.enums;

import cn.hutool.core.util.StrUtil;

public enum UserTypeEnum {
    ADMIN("管理员"),
    USER("系统会员"),
    UNKNOW("未知");

    //成员变量
    private String userType;

    UserTypeEnum(String userType) {
        this.userType = userType;
    }
    public static UserTypeEnum getByType(String type) {
        if (StrUtil.isEmpty(type)) {
            return UserTypeEnum.UNKNOW;
        }
        for (UserTypeEnum ut : UserTypeEnum.values()) {
            if (ut.toString().equalsIgnoreCase(type)) {
                return ut;
            }
        }
        return UserTypeEnum.UNKNOW;
    }

    public String getUserType() {
        return userType;
    }
}
