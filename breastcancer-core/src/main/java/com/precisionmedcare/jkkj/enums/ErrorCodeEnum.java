package com.precisionmedcare.jkkj.enums;

import com.baomidou.mybatisplus.extension.api.IErrorCode;

public enum ErrorCodeEnum implements IErrorCode {
    FAILED(-1L, "操作失败"),
    SUCCESS(0L, "执行成功"),
    ATTRIBUTE_IS_NULL(1000, "属性为null或空"),
    ARRAY_CANNOT_BE_EMPTY(1001, "数组不能为空"),
    OBJECT_CANNOT_BE_EMPTY(1002, "对象不能为空"),
    IS_TRUE_PASS(1003, "业务代码true通过"),
    UPLOAD_FILE_IS_EMPTY(1004, "上传文件为空"),
    FILE_FORM_ERROR(1005, "上传文件为空"),
    ATTRIBUTE_IS_NOT_NULL(1006, "属性不为null或空"),
    USERNAME_ALREADY_EXISTS(1050, "用户名已经存在"),
    PASSWORD_ENCRYPTION_FAILED(1051, "密码加密失败"),
    INCORRECT_ACCOUNT(1052, "账号不正确"),
    PASSWORD_DECRYPTION_FAILED(1053, "密码解密失败"),
    INCORRECT_PASSWORD(1054, "密码不正确"),
    IC_DOES_NOT_EXIST(1100, "ic卡不存在"),
    UNBOUND_IC_CARD(1101, "该用户未绑定精康检测卡"),
    JK_CARD_TIMES_USER_UP(1102, "精康检测卡次数已用完，请联系管管理员"),
    JK_CARD_HAVE_ALREADY_BIND(1103, "该ic卡已经被绑定"),
    ;
    private long code;
    private String msg;

    ErrorCodeEnum(long code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public long getCode() {
        return this.code;
    }

    public static ErrorCodeEnum fromCode(long code) {
        ErrorCodeEnum[] ecs = values();
        ErrorCodeEnum[] var3 = ecs;
        int var4 = ecs.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            ErrorCodeEnum ec = var3[var5];
            if (ec.getCode() == code) {
                return ec;
            }
        }

        return SUCCESS;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }

    public String toString() {
        return String.format("{\"code\": %s, \"msg\":\"%s\"}", this.code, this.msg);
    }
}
