package com.precisionmedcare.jkkj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.precisionmedcare.jkkj.entity.SysResult;

/**
 * (SysResult)表服务接口
 *
 * @author madison
 * @since 2021-01-04 14:19:58
 */
public interface SysResultService extends IService<SysResult> {

}
