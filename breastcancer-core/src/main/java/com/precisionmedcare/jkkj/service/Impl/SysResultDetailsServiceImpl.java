package com.precisionmedcare.jkkj.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.precisionmedcare.jkkj.entity.SysResultDetails;
import com.precisionmedcare.jkkj.mapper.SysResultDetailsMapper;
import com.precisionmedcare.jkkj.service.SysResultDetailsService;
import org.springframework.stereotype.Service;

/**
 * (SysResultDetails)表服务实现类
 *
 * @author madison
 * @since 2021-01-04 14:22:26
 */
@Service("sysResultDetailsService")
public class SysResultDetailsServiceImpl extends ServiceImpl<SysResultDetailsMapper, SysResultDetails> implements SysResultDetailsService {

}
