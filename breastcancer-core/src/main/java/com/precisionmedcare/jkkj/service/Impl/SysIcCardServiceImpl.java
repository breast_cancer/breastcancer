package com.precisionmedcare.jkkj.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.precisionmedcare.jkkj.entity.SysIcCard;
import com.precisionmedcare.jkkj.mapper.SysIcCardMapper;
import com.precisionmedcare.jkkj.service.SysIcCardService;
import org.springframework.stereotype.Service;

/**
 * (SysIcCard)表服务实现类
 *
 * @author madison
 * @since 2020-12-28 14:50:43
 */
@Service("sysIcCardService")
public class SysIcCardServiceImpl extends ServiceImpl<SysIcCardMapper, SysIcCard> implements SysIcCardService {

}
