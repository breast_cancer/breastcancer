package com.precisionmedcare.jkkj.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.precisionmedcare.jkkj.entity.SysUser;
import com.precisionmedcare.jkkj.mapper.SysUserMapper;
import com.precisionmedcare.jkkj.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * (SysUser)表服务实现类
 *
 * @author madison
 * @since 2020-12-11 17:09:36
 */
@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
