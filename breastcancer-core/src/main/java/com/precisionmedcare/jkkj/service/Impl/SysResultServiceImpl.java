package com.precisionmedcare.jkkj.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.precisionmedcare.jkkj.entity.SysResult;
import com.precisionmedcare.jkkj.mapper.SysResultMapper;
import com.precisionmedcare.jkkj.service.SysResultService;
import org.springframework.stereotype.Service;

/**
 * (SysResult)表服务实现类
 *
 * @author madison
 * @since 2021-01-04 14:19:58
 */
@Service("sysResultService")
public class SysResultServiceImpl extends ServiceImpl<SysResultMapper, SysResult> implements SysResultService {

}
