package com.precisionmedcare.jkkj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.precisionmedcare.jkkj.entity.SysIcCard;

/**
 * (SysIcCard)表服务接口
 *
 * @author madison
 * @since 2020-12-28 14:50:42
 */
public interface SysIcCardService extends IService<SysIcCard> {

}
