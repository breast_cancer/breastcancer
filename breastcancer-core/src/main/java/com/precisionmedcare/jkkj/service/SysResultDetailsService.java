package com.precisionmedcare.jkkj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.precisionmedcare.jkkj.entity.SysResultDetails;

/**
 * (SysResultDetails)表服务接口
 *
 * @author madison
 * @since 2021-01-04 14:22:26
 */
public interface SysResultDetailsService extends IService<SysResultDetails> {

}
