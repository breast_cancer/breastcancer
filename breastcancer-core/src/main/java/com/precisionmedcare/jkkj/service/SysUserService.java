package com.precisionmedcare.jkkj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.precisionmedcare.jkkj.entity.SysUser;

/**
 * (SysUser)表服务接口
 *
 * @author madison
 * @since 2020-12-11 17:09:34
 */
public interface SysUserService extends IService<SysUser> {

}
