package com.precisionmedcare.jkkj.vo;

import com.precisionmedcare.jkkj.entity.SysUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
  * @return
  * @author qlh
  * @公众号 madison龙少
  * @creed: Talk is cheap,show me the code
  * @date
  */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUserVo{
    private SysUser sysUser;
    private String token;
}
