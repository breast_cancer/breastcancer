package com.precisionmedcare.jkkj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.precisionmedcare.jkkj.entity.SysResult;
import org.apache.ibatis.annotations.Mapper;

/**
 * (SysResult)表数据库访问层
 *
 * @author madison
 * @since 2021-01-04 14:19:57
 */
@Mapper
public interface SysResultMapper extends BaseMapper<SysResult> {

}
