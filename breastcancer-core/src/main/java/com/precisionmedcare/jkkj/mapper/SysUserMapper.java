package com.precisionmedcare.jkkj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.precisionmedcare.jkkj.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * (SysUser)表数据库访问层
 *
 * @author madison
 * @since 2020-12-11 17:09:32
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
