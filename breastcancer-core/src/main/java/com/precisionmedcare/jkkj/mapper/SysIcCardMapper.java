package com.precisionmedcare.jkkj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.precisionmedcare.jkkj.entity.SysIcCard;
import org.apache.ibatis.annotations.Mapper;

/**
 * (SysIcCard)表数据库访问层
 *
 * @author madison
 * @since 2020-12-28 14:50:42
 */
@Mapper
public interface SysIcCardMapper extends BaseMapper<SysIcCard> {

}
