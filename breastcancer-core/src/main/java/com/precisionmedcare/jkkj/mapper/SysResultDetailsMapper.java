package com.precisionmedcare.jkkj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.precisionmedcare.jkkj.entity.SysResultDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * (SysResultDetails)表数据库访问层
 *
 * @author madison
 * @since 2021-01-04 14:22:26
 */
@Mapper
public interface SysResultDetailsMapper extends BaseMapper<SysResultDetails> {

}
