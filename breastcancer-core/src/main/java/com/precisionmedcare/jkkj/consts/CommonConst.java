package com.precisionmedcare.jkkj.consts;
/**
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @creed: Talk is cheap,show me the code
 * @date
 */
public class CommonConst {
    /**
     * 安全密码(UUID生成)，作为盐值用于用户密码的加密
     */
    public static final String MADISON_SECURITY_KEY = "929123f8f17944e8b0a531045453e1f1";

    /**
     * 程序默认的错误状态码
     */
    public static final int DEFAULT_ERROR_CODE = 500;

    /**
     * 程序默认的成功状态码
     */
    public static final int DEFAULT_SUCCESS_CODE = 200;

    /**
     *  正常：
     *      通用状态码
     */
    public static final int GLOBAL_NORMAL_STATE = 1;

    /**
     * 禁用：
     * 通用状态码disabled
     */
    public static final int GLOBAL_DISABLED_STATE = 0;
}
