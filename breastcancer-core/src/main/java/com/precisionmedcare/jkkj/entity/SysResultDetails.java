package com.precisionmedcare.jkkj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * (SysResultDetails)表实体类
 *
 * @author madison
 * @since 2021-01-04 14:22:26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SysResultDetails对象", description = "$tableInfo.comment")
public class SysResultDetails extends Model<SysResultDetails> {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("主键id")
    private Integer id;
    @ApiModelProperty("计算结果id")
    private Integer resultId;
    /**
     * 文件上传的路径
     */
    @ApiModelProperty("文件上传的路径")
    private String uploadPath;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
