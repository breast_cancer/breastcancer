package com.precisionmedcare.jkkj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (SysUser)表实体类
 *
 * @author madison
 * @since 2020-12-11 17:09:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SysUser对象", description = "$tableInfo.comment")
public class SysUser extends Model<SysUser> {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("$column.comment")
    private Integer id;
    @ApiModelProperty("$column.comment")
    private String username;
    /**
     * 登录密码
     */
    @ApiModelProperty("登录密码")
    private String password;
    /**
     * 昵称
     */
    @ApiModelProperty("昵称")
    private String nickname;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;
    /**
     * 头像地址
     */
    @ApiModelProperty("头像地址")
    private String avatar;
    /**
     * 管理员、普通用户
     */
    @ApiModelProperty("管理员、普通用户")
    private String userType;
    /**
     * 用户状态:1正常 0禁用
     */
    @ApiModelProperty("用户状态:1正常 0禁用")
    private Integer status;

    public SysUser(String username){

    }
    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
