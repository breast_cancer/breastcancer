package com.precisionmedcare.jkkj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * (SysIcCard)表实体类
 *
 * @author madison
 * @since 2020-12-28 14:50:42
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SysIcCard对象", description = "$tableInfo.comment")
public class SysIcCard extends Model<SysIcCard> {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("$column.comment")
    private Integer id;
    /**
     * ic卡号
     */
    @ApiModelProperty("ic卡号")
    private String number;
    /**
     * ic_card剩余次数
     */
    @ApiModelProperty("ic_card剩余次数")
    private Integer count;
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private Integer userId;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
