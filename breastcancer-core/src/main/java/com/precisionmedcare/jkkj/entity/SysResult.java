package com.precisionmedcare.jkkj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * (SysResult)表实体类
 *
 * @author madison
 * @since 2021-01-04 14:19:57
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SysResult对象", description = "分析结果")
public class SysResult extends Model<SysResult> {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键id", required = true)
    private Integer id;
    @ApiModelProperty("检测id")
    private String detectionId;
    @ApiModelProperty("病人id")
    private String patientId;
    @ApiModelProperty("上传用户名")
    private String uploaderUsername;
    @ApiModelProperty("上传用户id")
    private Integer userId;
    /**
     * 上传时间
     */
    @ApiModelProperty("上传时间")
    private Date uploadTime;
    /**
     * 计算结果
     */
    @ApiModelProperty("计算结果")
    private String result;
    /**
     * 医生建议
     */
    @ApiModelProperty("医生建议")
    private String doctorNotes;
    /**
     * 用户ip
     */
    @ApiModelProperty("用户ip")
    private String userIp;
    /**
     * 错误信息
     */
    @ApiModelProperty("错误信息")
    private String errorInfo;
    /**
     * 状态：0表示被删除 1表示正常 默认1
     */
    @ApiModelProperty("状态：0表示被删除 1表示正常 默认1")
    private Integer status;

    /**
     * 上传文件路径
     */
    @ApiModelProperty(value = "上传文件路径")
    @TableField(exist = false)
    private List<String> filePath;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
