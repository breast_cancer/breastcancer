package com.precisionmedcare.jkkj.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
@Data
@ApiModel(value = "UploadDTO",description = "上传需要对象")
public class UploadDTO {
    @ApiModelProperty(name = "status", value = "上传选项：上传头像：status='avatar',上传视频：status='video'...", required = true, dataType = "String")
    private String status;
    @ApiModelProperty(name = "oldAvatar", value = "旧头像", required = true, dataType = "String")
    private String oldAvatar;
    @ApiModelProperty(name = "uuid", value = "每次上传检测的唯一确定文件夹名称", required = true, dataType = "String")
    private String uuid;
}
