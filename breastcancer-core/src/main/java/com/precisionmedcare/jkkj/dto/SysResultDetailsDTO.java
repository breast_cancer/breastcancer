package com.precisionmedcare.jkkj.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (SysResultDetails)表实体类
 *
 * @author madison
 * @since 2021-01-04 14:36:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
@ApiModel(value = "SysResultDetails对象", description = "计算内容")
public class SysResultDetailsDTO implements Serializable {
    @ApiModelProperty("主键id")
    private Integer id;
    @ApiModelProperty("结果id")
    private Integer resultId;
    /**
     * 上传路径
     */
    @ApiModelProperty("上传路径")
    private String uploadPath;


}
