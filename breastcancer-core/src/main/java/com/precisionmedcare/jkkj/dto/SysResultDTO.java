package com.precisionmedcare.jkkj.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * (SysResult)表实体类
 *
 * @author madison
 * @since 2021-01-04 14:36:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
@ApiModel(value = "SysResult对象", description = "分析结果对象")
public class SysResultDTO implements Serializable {
    @ApiModelProperty(value = "主键id", hidden = true)
    private Integer id;
    @ApiModelProperty(value = "检测id", required = true)
    private String detectionId;
    @ApiModelProperty(value = "病人id", required = true)
    private String patientId;
    @ApiModelProperty(value = "用户名", required = true)
    private String uploaderUsername;
    @ApiModelProperty(value = "用户id", required = true)
    private Integer userId;
    /**
     * 上传时间
     */
    @ApiModelProperty(value = "上传时间", hidden = true)
    private Date uploadTime;
    /**
     * 计算结果
     */
    @ApiModelProperty(value = "计算结果", hidden = true)
    private String result;
    /**
     * 医生建议
     */
    @ApiModelProperty(value = "医生建议", hidden = true)
    private String doctorNotes;
    /**
     * 用户ip
     */
    @ApiModelProperty(value = "用户ip", hidden = true)
    private String userIp;
    /**
     * 错误信息
     */
    @ApiModelProperty(value = "错误信息", hidden = true)
    private String errorInfo;
    /**
     * 状态：0表示被删除 1表示正常 默认1
     */
    @ApiModelProperty(value = "状态：0表示被删除 1表示正常 默认1", hidden = true)
    private Integer status;
    /**
     * 上传文件路径
     */
    @ApiModelProperty(value = "上传文件路径", required = true)
    private List<String> filePath;

}
