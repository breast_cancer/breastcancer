package com.precisionmedcare.jkkj.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.bytebuddy.implementation.bind.annotation.Empty;

import java.io.Serializable;

/**
 * (SysUser)表实体类
 *
 * @author madison
 * @since 2020-12-11 17:09:38
 */
@Data
@SuppressWarnings("serial")
@ApiModel(value = "SysUserDTO",description = "SysUserDTO:接收前台传输用户对象")
public class SysUserDTO implements Serializable {

    @ApiModelProperty(value = "id:对应主键id（新增：id=0。修改：id=需要修改的id）", required = true)
    private Integer id;
    @ApiModelProperty(value = "username:新增：必填，修改：用户名可修改可不修改", required = false)
    private String username;
    /**
     * 登录密码
     */
    @ApiModelProperty(value = "password:新增：必填，修改：可填可不填(无需加密 后端加密)", required = false)
    private String password;
    /**
     * 昵称
     */
    @ApiModelProperty(value = "nickname:新增：可不填 默认初始化为用户名，修改：可填可不填", required = false)
    private String nickname;
    /**
     * 备注
     */
    @ApiModelProperty(value = "remark:新增：可不填 默认为空，修改：可填可不填", required = false)
    private String remark;
    /**
     * 头像地址
     */
    @ApiModelProperty(value = "avatar:新增：可不填 默认服务器一张图片，修改：可填可不填", required = false)
    private String avatar;
    /**
     * 管理员、普通用户
     */
    @ApiModelProperty(value = "userType:新增：可不填 默认USER类型，修改：可填可不填（ADMIN/USER/....）", required = false)
    private String userType;
    /**
     * 用户状态:1正常 0禁用
     */
    @ApiModelProperty(value = "status:新增：无需填 默认正常，修改：可填可不填（1正常 0禁用）", required = false)
    private Integer status;


}
