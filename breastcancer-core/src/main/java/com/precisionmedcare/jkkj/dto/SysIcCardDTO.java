package com.precisionmedcare.jkkj.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (SysIcCard)表实体类
 *
 * @author madison
 * @since 2020-12-28 14:50:43
 */
@Data
@SuppressWarnings("serial")
@ApiModel(value = "SysIcCardDTO",description = "SysIcCardDTO:接收前台传输ic卡对象")
public class SysIcCardDTO implements Serializable {

    @ApiModelProperty(value = "id:对应主键id（新增：id=0。修改：id=需要修改的id）", required = true)
    private Integer id;
    /**
     * ic卡号
     */
    @ApiModelProperty(value = "ic卡号:(新增:必传 修改：不传，该参数不能修改)",required = true)
    private String number;
    /**
     * ic_card剩余次数
     */
    @ApiModelProperty(value = "可供使用次数(新增：不传的话保存数据库默认100次。修改：修改就传，不修改就不传)",required = false)
    private Integer count;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户绑定该卡的用户id（新增：不传默认保存数据库为0-即该卡没有绑用户。修改：修改就传，不修改就不传）",required = false)
    private Integer userId;


}
