SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `id`        bigint(20) UNSIGNED                                              NOT NULL AUTO_INCREMENT,
    `username`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci          NULL DEFAULT NULL,
    `password`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci          NULL DEFAULT NULL COMMENT '登录密码',
    `nickname`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci           NULL DEFAULT '' COMMENT '昵称',
    `remark`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci          NULL DEFAULT '' COMMENT '备注',
    `avatar`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci          NULL DEFAULT NULL COMMENT '头像地址',
    `user_type` enum ('ADMIN','USER') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'USER' COMMENT '管理员、普通用户',
    `status`    int(1) UNSIGNED                                                  NULL DEFAULT 1 COMMENT '用户状态:1正常 0禁用',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 0
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;
drop table if exists `sys_ic_card`;
create table `sys_ic_card`
(
    `id`        bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `number`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ic卡号',
    `count`   int(11)      DEFAULT 100 COMMENT 'ic_card剩余次数',
    `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 0
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT=COMPACT;

DROP TABLE IF EXISTS `sys_result`;
CREATE TABLE `sys_result`
(
    `id`           bigint(20) UNSIGNED                                     NOT NULL AUTO_INCREMENT,
    `detection_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    `patient_id`   varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    `uploader_username`     varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    `user_id`      bigint(20) UNSIGNED NOT NULL,
    `upload_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '上传时间',
    `result` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '计算结果',
    `doctor_notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '医生建议',
    `user_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '用户ip',
    `error_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '错误信息',
    `status` int(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态：0表示被删除 1表示正常 默认1',
    PRIMARY KEY (`id`) USING BTREE
)ENGINE = InnoDB
 AUTO_INCREMENT = 0
 CHARACTER SET = utf8
 COLLATE = utf8_general_ci
 ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `sys_result_details`;
CREATE TABLE `sys_result_details`
(
    `id`           bigint(20) UNSIGNED                                     NOT NULL AUTO_INCREMENT,
    `result_id`      bigint(20) UNSIGNED NOT NULL,
    `upload_path` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '上传路径',
    PRIMARY KEY (`id`) USING BTREE
)ENGINE = InnoDB
 AUTO_INCREMENT = 0
 CHARACTER SET = utf8
 COLLATE = utf8_general_ci
 ROW_FORMAT = Dynamic;
